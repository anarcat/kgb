class { 'kgb':
  channels     => [
    {
      'name'      => '#kgb-puppet',
      'network'   => 'freenode',
      'broadcast' => 'yes',
    },
  ],
  networks     => {
    'freenode' => {
      'nick'              => 'KGB-puppet',
      'ircname'           => 'KGB-puppet',
      'username'          => 'kgb-puppet',
      'password'          => '~',
      'nickserv_password' => '~',
      'server'            => 'irc.freenode.net',
    },
  },
  repositories => {
    'test' => {
      'password' => 'test',
    },
  },
  realize_tag  => 'test-tag',
}
kgb::client { '/tmp/client-test.conf':
  repo_id    => 'test',
  password   => 'test',
  export_tag => 'test-tag',
}
# then kgb-client --conf /tmp/client-test.conf --relay-msg test will DTRT
