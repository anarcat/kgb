# KGB

## Description

The `KGB` module sets up a [KGB bot](https://salsa.debian.org/kgb-team) that can be used to broadcast
various announcements on IRC.

## Setup

When the server is configured, it will install the `kgb-bot` package,
overwrite its config and start the service.

## Usage

To setup a bot with the default configuration:

    include kgb

The default configuration is not very useful, and in fact KGB does not
start at all in its default configuration. You will at least need to
specify a network and repository:

    class { 'kgb':
      channels => [
        {
          'name'      => '#kgb-puppet',
          'network'   => 'freenode',
          'broadcast' => 'yes',
        },
      ],
      networks => {
        'freenode' => {
          'nick' => 'KGB-puppet',
          'ircname' => 'KGB-puppet',
          'username' => 'kgb-puppet',
          'server' => 'irc.freenode.net',
        },
      },
      repositories => {
        'test' => {
          'password' => 'test',
        },
      },
    }

By default, the server will listen only locally, use the
`$listen_address` and `$listen_port` parameters to change that:

    class { 'kgb':
        listen_address => '*',
        listen_port    => '80',
        # ...
    }

Note that KGB does not support HTTPS, so you might prefer to put a
proxy in front of it to implement that.

You can also configure a client with a matching repository and
password, for example to match the above:

    kgb::client { '/tmp/client-test.conf':
      repo_id  => 'test',
      password => 'test',
    }

Then a test notification can be sent with:

    kgb-client --conf /tmp/client-test.conf --relay-msg test

## Limitations

A public listening address is not useful and could possibly be
removed. But since KGB only supports cleartext HTTP, it might be
preferable to hook into the Nginx module or similar to setup a more
secure HTTPS proxy.

There are no unit tests.

# Reference

The full reference documentation for this module may be found at on
[GitLab Pages][pages].

Alternatively, you may build yourself the documentation using the
`puppet strings generate` command. See the documentation for
[Puppet Strings][strings] for more information.

[pages]: https://shared-puppet-modules-group.gitlab.io/kgb
[strings]: https://puppet.com/blog/using-puppet-strings-generate-great-documentation-puppet-modules
